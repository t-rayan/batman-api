const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = mongoose.Schema({
    title: { type: String, trim: true, required: true},
    description: { type:String, required: true },
    // author: { type:String, required: true },
    created_at: { type: Date, default: Date.now },
});

const Post = module.exports = mongoose.model('Post', PostSchema);
