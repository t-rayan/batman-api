const mongoose = require('mongoose');

const AuthorSchema = mongoose.Schema({
    fullname: { type: String, trim: true, required: true},
    address: { type: String, required: true },
    phone: { type: Number, required: true },
    email: { type: String, required: true },
    created_at: { type: Date, default: Date.now }
});

const Author = module.exports = mongoose.model('Author', AuthorSchema);
