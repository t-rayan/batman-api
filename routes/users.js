const express = require('express');
const router = express.Router();
const User = require('../models/user');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/dbConfig');
var mongoose = require('mongoose');

/* register */
router.post('/register', (req, res, next) => {

  //sample user
  let newUser = new User({
    fullname: req.body.fullname,
    username: req.body.username,
    password: req.body.password,
    email: req.body.email,
    isAdmin: false
  });
  console.log(newUser);
  User.addUser(newUser, (err, user) => {
    if(err) {
      res.json({
        success: true,
        msg: "Failed to register user"
      });
    } else {
      res.json({
        success: true,
        msg: "User registered"
      });
    }
  });


  });

//authenticate 
router.post('/authenticate', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  User.getUserByUsername(username, (err, user) => {
    if(err) throw err;
    if(!user) {
      console.log(username);
      return res.json({success: false, msg: "User not found"});
      
    } 

    User.comparePassword(password, user.password, (err, isMatch) => {
      if(err) throw err;
      if(isMatch) {
        const token = jwt.sign(user, config.secret, {
          expiresIn: 604800
        });

        res.json({
          success: true,
          token: "JWT" + " " + token,
          user: {
            id:user._id,
            name:user.fullname,
            username: user.username,
            email: user.email,
            isAdmin: user.isAdmin
          },
          role: user.isAdmin
        });
      } else {
        return res.json({success:false, msg: 'Wrong password'});
      }
    });
  });
});

//profile
router.get('/profile', passport.authenticate('jwt', {session: false}), isAdmin, (req, res, next) => {
  res.json({user: req.user});
});

//all users
router.get('/all', (req, res, next) => {
  User.find(function(err, users) {
    if(err) return err;
    res.json(users);
  });
});

router.get('/:_id', (req, res, next) => {
  User.findById(function(err, user) {
    if(err) return err;
    console.log(user);
    res.json(user);
  })
})

function isAdmin(req, res, next) {
  const user = req.user;
 console.log(user.email);
 if(user.isAdmin) {
   next();
 } else {
   console.log('not admin');
 }
}

module.exports = router;
