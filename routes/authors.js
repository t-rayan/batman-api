var express = require('express');
var router = express.Router();
var Authors = require('../models/authors');

/* GET home page. */
router.get('/', (req, res) => {
  Authors.find((err, authors) => {
    if(err){
      throw err;
    } else {
      res.json(authors);
    }
  })
})

router.post('/', (req, res, next) => {

  let newAuthor = new Authors({
    fullname: req.body.fullname,
    address: req.body.address,
    phone: req.body.phone,
    email: req.body.email
  });

  newAuthor.save((err, user) => {
    if(err) {
      res.json({
        success: false,
        msg: "Failed to add author"
      });
    } else {
      res.json({
        success: true,
        msg: "Author added successfully"
      });
    }
  }); 

});

module.exports = router;
