var express = require('express');
var router = express.Router();
var Posts = require('../models/posts');
var Authors = require('../models/authors');


router.get('/', (req, res) => {
	Posts.find((err, posts) => {
		if (err) throw err;
		res.json(posts)
	})
});

router.post('/', (req, res) => {

	if (!req.body.title) {
		res.json({
			success: false,
			msg: 'Title field is required'
		});
	} else if (!req.body.description) {
		res.json({
			success: false,
			msg: 'Description is required'
		});
	} else {
		const newPost = new Posts();

		newPost.title = req.body.title;
		newPost.description = req.body.description;
		// newPost.author = req.body.author;

		newPost.save((err, post) => {
			if (err) throw err;
			res.json({
				success: true,
				msg: "Post added successfully"
			});
		});
	}
});

router.delete('/:id', (req, res) => {
	Posts.findByIdAndRemove(_id = req.params.id, (err, posts) => {
		if (err)
			throw err;

		res.json({
			success: true,
			msg: "Post Deleted Successfully",
			posts: posts
		});
	});
});

router.get('/:id', (req, res) => {
	Posts.findById(_id = req.params.id, (err, post) => {
		if (err) throw err;
		res.json(post)
	})
});

router.put('/:id', (req, res) => {
	Posts.findById(_id = req.params.id, (err, post) => {
		if (err) throw err;

		//update the post
		post.title = req.body.title;
		post.description = req.body.description;

		//saving the post
		post.save((err) => {
			if (err) throw err;

			res.json({
				success: true,
				msg: "Post Updated Successfully",
				post: post
			});
		});
	});
});

module.exports = router;